#include "simulate_bayer_mosaic.h"

void simulate_bayer_mosaic(
  const std::vector<unsigned char> & rgb,
  const int & width,
  const int & height,
  std::vector<unsigned char> & bayer)
{
  bayer.resize(width*height);
  ////////////////////////////////////////////////////////////////////////////
  // Add your code here

    //switch to decide color for the index
  
	for(int i = 0; i < height; i++){
		for(int j = 0; j<width; j++){
			int bayer_position = i%2 + j%2;
			switch(bayer_position) {
				case 0:
				  bayer[i*width +j] = rgb[2+3*(i*width + j)];
				  break; 

				case 1:
				  bayer[i*width +j] = rgb[1+3*(i*width + j)];
				  break;

				case 2:
				  bayer[i*width +j] = rgb[0+3*(i*width + j)];
				  break;
			}
		}
	}
  ////////////////////////////////////////////////////////////////////////////
}

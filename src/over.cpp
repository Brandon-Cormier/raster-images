#include "over.h"

void over(
  const std::vector<unsigned char> & A,
  const std::vector<unsigned char> & B,
  const int & width,
  const int & height,
  std::vector<unsigned char> & C)
{
  C.resize(A.size());
  ////////////////////////////////////////////////////////////////////////////
  // Add your code here
  for (int i = 0; i < height; i++){
    for( int j = 0; j < width*4; j+=4){

      
      double src_r = A[i*width*4 + j + 0];
      double src_g = A[i*width*4 + j + 1];
      double src_b = A[i*width*4 + j + 2];
      double src_a = A[i*width*4 + j + 3]/255.0;

      
      double  dest_r = B[i*width*4 + j + 0];
      double  dest_g = B[i*width*4 + j + 1];
      double  dest_b = B[i*width*4 + j + 2];
      double  dest_a = B[i*width*4 + j + 3]/255.0;

      //https://en.wikipedia.org/wiki/Alpha_compositing

      double  out_a = src_a + dest_a * (1-src_a);
      double  out_r = 0;
      double  out_g = 0;
      double  out_b = 0;



      if (out_a != 0){
        out_r = ((src_r * src_a + dest_r * dest_a * (1-src_a)) / out_a);
        out_g = ((src_g * src_a + dest_g * dest_a * (1-src_a)) / out_a);
        out_b = ((src_b * src_a + dest_b * dest_a * (1-src_a)) / out_a);
      }

      C[i*width*4 + j + 0] = (unsigned char)((int)out_r);
      C[i*width*4 + j + 1] = (unsigned char)((int)out_g);
      C[i*width*4 + j + 2] = (unsigned char)((int)out_b);
      C[i*width*4 + j + 3] = (unsigned char)((int)out_a)*255.0;
    }
  }
  ////////////////////////////////////////////////////////////////////////////
}

#include "rotate.h"

void rotate(
  const std::vector<unsigned char> & input,
  const int width,
  const int height,
  const int num_channels,
  std::vector<unsigned char> & rotated)
{
  rotated.resize(height*width*num_channels);
  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////

  //go from the bottom left of rotated up the height and then to the next row
  //go from 0 to size on input
   for(int i = 0; i < height; i++){
    int rotate = height*(width-1)*num_channels;
    for(int j = 0; j<width*num_channels; j+= num_channels){
      for(int z = 0; z < num_channels; z++){
        rotated[rotate + i*num_channels + z] = input[i*width*num_channels + j + z];
      }
      rotate -= height*num_channels;
    }
  }
  ////////////////////////////////////////////////////////////////////////////
}

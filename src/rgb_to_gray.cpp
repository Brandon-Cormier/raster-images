#include "rgb_to_gray.h"

void rgb_to_gray(
  const std::vector<unsigned char> & rgb,
  const int width,
  const int height,
  std::vector<unsigned char> & gray)
{
  gray.resize(height*width);
  const int num_channels = 3;
  ////////////////////////////////////////////////////////////////////////////
  // Add your code here

    //loop through every channel and update gray[] with the weighted average of rgb
    for(int i = 0; i < height; i++){
    	for(int j = 0; j<width*num_channels; j+= num_channels){
    		double grayscale = 0;
   	  		grayscale += 0.2126*rgb[i*width*num_channels + j];
   	  		grayscale += 0.7152 * rgb[i*width*num_channels + j + 1];
   	  		grayscale += 0.0722 * rgb[i*width*num_channels + j + 2];
   	  		gray[i*width + j/num_channels] = grayscale;
    	}
  }
  ////////////////////////////////////////////////////////////////////////////
}



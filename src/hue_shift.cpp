#include "hue_shift.h"
#include "hsv_to_rgb.h"
#include "rgb_to_hsv.h"
#include <algorithm>
#include <cmath>

void hue_shift(
  const std::vector<unsigned char> & rgb,
  const int width,
  const int height,
  const double shift,
  std::vector<unsigned char> & shifted)
{
  shifted.resize(rgb.size());
  ////////////////////////////////////////////////////////////////////////////
  // Add your code here
  for (int i=0; i<rgb.size(); i+=3){
  	double r = rgb[i];
  	double g = rgb[i+1];
  	double b = rgb[i+2];
  	double h;
  	double s;
  	double v;

  	rgb_to_hsv(r,g,b,h,s,v);

  	h += shift;

    //maing sure it didn't overflow
    
  	h = std::fmod(h,360);

  	hsv_to_rgb(h,s,v,r,g,b);

  	shifted[i] = r;
  	shifted[i+1] = g;
  	shifted[i+2] = b;


  }

  ////////////////////////////////////////////////////////////////////////////
}
